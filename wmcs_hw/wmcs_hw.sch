EESchema Schematic File Version 4
LIBS:wmcs_hw-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF:NRF24L01_Breakout U1
U 1 1 5D7BFBAA
P 5700 1850
F 0 "U1" H 6080 1896 50  0000 L CNN
F 1 "NRF24L01_Breakout" H 6080 1805 50  0000 L CNN
F 2 "RF_Module:nRF24L01_Breakout" H 5850 2450 50  0001 L CIN
F 3 "http://www.nordicsemi.com/eng/content/download/2730/34105/file/nRF24L01_Product_Specification_v2_0.pdf" H 5700 1750 50  0001 C CNN
	1    5700 1850
	1    0    0    -1  
$EndComp
Text Label 1000 2300 0    50   ~ 0
SPI0_CS
Wire Wire Line
	1400 2300 1000 2300
Wire Wire Line
	1400 2400 1000 2400
Wire Wire Line
	1400 2500 1000 2500
Wire Wire Line
	1400 2600 1000 2600
Text Label 1000 2400 0    50   ~ 0
SPI0_MOSI
Text Label 1000 2500 0    50   ~ 0
SPI0_MISO
Text Label 1000 2600 0    50   ~ 0
SPI0_SCK
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 5D7AC385
P 1900 1900
F 0 "A1" H 1900 811 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 1900 720 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2050 950 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 1900 900 50  0001 C CNN
	1    1900 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5D7CE711
P 1950 3250
F 0 "#PWR01" H 1950 3000 50  0001 C CNN
F 1 "GND" H 1955 3077 50  0000 C CNN
F 2 "" H 1950 3250 50  0001 C CNN
F 3 "" H 1950 3250 50  0001 C CNN
	1    1950 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 2900 1950 2900
Wire Wire Line
	1950 3250 1950 2900
Connection ~ 1950 2900
Wire Wire Line
	1950 2900 2000 2900
$Comp
L power:GND #PWR04
U 1 1 5D7D36A1
P 5700 2600
F 0 "#PWR04" H 5700 2350 50  0001 C CNN
F 1 "GND" H 5705 2427 50  0000 C CNN
F 2 "" H 5700 2600 50  0001 C CNN
F 3 "" H 5700 2600 50  0001 C CNN
	1    5700 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2600 5700 2450
Wire Wire Line
	2100 900  2650 900 
$Comp
L power:+5V #PWR03
U 1 1 5D7D4B40
P 2650 900
F 0 "#PWR03" H 2650 750 50  0001 C CNN
F 1 "+5V" H 2665 1073 50  0000 C CNN
F 2 "" H 2650 900 50  0001 C CNN
F 3 "" H 2650 900 50  0001 C CNN
	1    2650 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR02
U 1 1 5D7D5407
P 2000 800
F 0 "#PWR02" H 2000 650 50  0001 C CNN
F 1 "+3V3" H 2015 973 50  0000 C CNN
F 2 "" H 2000 800 50  0001 C CNN
F 3 "" H 2000 800 50  0001 C CNN
	1    2000 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 800  2000 900 
Text Label 4750 1550 0    50   ~ 0
SPI0_MOSI
Wire Wire Line
	5200 1550 4750 1550
Text Label 4750 1650 0    50   ~ 0
SPI0_MISO
Text Label 4750 1750 0    50   ~ 0
SPI0_SCK
Text Label 4750 1850 0    50   ~ 0
SPI0_CS
Wire Wire Line
	5200 1650 4750 1650
Wire Wire Line
	5200 1750 4750 1750
Wire Wire Line
	5200 1850 4750 1850
$Comp
L power:+3V3 #PWR?
U 1 1 5D7E5852
P 5700 1050
F 0 "#PWR?" H 5700 900 50  0001 C CNN
F 1 "+3V3" H 5715 1223 50  0000 C CNN
F 2 "" H 5700 1050 50  0001 C CNN
F 3 "" H 5700 1050 50  0001 C CNN
	1    5700 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1050 5700 1250
Text Label 4750 2050 0    50   ~ 0
NRF24_CE
Text Label 4750 2150 0    50   ~ 0
NRF24_IRQ
Wire Wire Line
	5200 2050 4750 2050
Wire Wire Line
	5200 2150 4750 2150
Text Label 1000 2000 0    50   ~ 0
NRF24_CE
Wire Wire Line
	1400 2000 1000 2000
Text Label 950  1500 0    50   ~ 0
NRF24_IRQ
Wire Wire Line
	1400 1500 950  1500
$EndSCHEMATC
